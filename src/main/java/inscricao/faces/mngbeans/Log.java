package inscricao.faces.mngbeans;

import java.util.Date;

public class Log {
	private String login;
	private Date hora;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public Date getHora() {
		return hora;
	}
	public void setHora(Date hora) {
		this.hora = hora;
	}
}
