package inscricao.faces.mngbeans;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import utfpr.faces.support.PageBean;

@ManagedBean
@RequestScoped
public class LoginBean extends PageBean {
	@ManagedProperty(value="#{registroLogin}")
	private RegistroLogin registro;
	private String login;
	private String senha;
	private boolean admin;
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public boolean getAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String entrar() {
		if(!this.login.equals(this.senha)) {
			  FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Acesso negado", null);
			  getFacesContext().addMessage(null, facesMessage);
			  return "login.xhtml";
		} else {
			Log log = new Log();
			log.setLogin(this.login);
			log.setHora(new Date());
			this.registro.getLogs().add(log);
			if(this.admin)
				return "admin.xhtml";
			else
				return "cadastro.xhtml";
		}
	}
	public RegistroLogin getRegistro() {
		return registro;
	}
	public void setRegistro(RegistroLogin registro) {
		this.registro = registro;
	}
}
