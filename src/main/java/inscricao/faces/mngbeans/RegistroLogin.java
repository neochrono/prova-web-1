package inscricao.faces.mngbeans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import utfpr.faces.support.ApplicationBean;

@ManagedBean
@ApplicationScoped
public class RegistroLogin extends ApplicationBean {
	private List<Log> logs = new ArrayList<Log>();
	public List<Log> getLogs() {
		return logs;
	}
	public void setLogs(List<Log> logs) {
		this.logs = logs;
	}
}
